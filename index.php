<?

$dir = dirname(__FILE__);

define('DIR', $dir);
require_once(DIR.'/class/number.php');


if (!isset($_GET['number'])) {
    $_GET['number'] = rand(0, 999999999);
}

$number = new number( $_GET['number'] , new russian() );
$numRus = $number->getNumber2Text();
$number->setLanguage(new english() );
$numEng = $number->getNumber2Text();

echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>'.$numRus."<br>".$numEng."<br>".$_GET['number'].'</body></html>';
