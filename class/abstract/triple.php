<?

abstract class abstract_triple {

    protected $_number;
    protected $_hundred;
    protected $_tens;
    protected $_one;

    public function getNumber() {
        return $this->_number;
    }
    public function setNumber($num) {
        return $this->_number = $num;
    }
    public function getParts() {
        $this->_hundred = floor($this->_number/100);
        $this->_tens = floor( ($this->_number - $this->_hundred*100)/10 );
        $this->_one = $this->_number - $this->_hundred*100 - $this->_tens*10;
        if ($this->_tens == 1) {
            $this->_tens = 0;
            $this->_one += 10;
        }
    }
    public function getHandred() {
        return $this->_hundred;
    }
    public function getTens() {
        return $this->_tens;
    }
    public function getOne() {
        return $this->_one;
    }

}