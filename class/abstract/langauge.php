<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Arkon
 * Date: 17.07.12
 * Time: 20:36
 * To change this template use File | Settings | File Templates.
 */
abstract class language
{
    const HUNDRED = 0;
    const THOUSAND = 1;
    const MILLIONS = 2;

    protected $_word = '';
    protected $_gender = false;
    protected $_category = false;

    abstract protected function getHundredName($i);
    abstract protected function getTensName($i);
    abstract protected function getOneName($i);
    abstract protected function getCategoryName($one);
    abstract public function getZero();

    /**
     * @param  array $numberCollection
     */
    public function getWordName(array $numberCollection) {
        $count = count($numberCollection)-1;
        if ($count > self::MILLIONS) {
            throw new Exception('number biger than 999 999 999');
        }
        for ($i = $count; $i >= self::HUNDRED; $i--) {
            $this->_category = $i;
            $this->_word .= ' '.$this->getHundredName($numberCollection[$i]->getHandred());
            $this->_word .= ' '.$this->getTensName($numberCollection[$i]->getTens());
            $this->_word .= ' '.$this->getOneName($numberCollection[$i]->getOne());
            $this->_word .= ' '.$this->getCategoryName($numberCollection[$i]->getOne());
        }
        return $this->_word;
    }
}
