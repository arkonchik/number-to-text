<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Arkon
 * Date: 17.07.12
 * Time: 20:38
 * To change this template use File | Settings | File Templates.
 */
require_once(DIR.'/class/abstract/langauge.php');
class english extends language
{

    private static $_hundredName = array(
        0 => '',
        1 => 'one hundred',
        2 => 'two hundred',
        3 => 'three hundred',
        4 => 'four hundred',
        5 => 'five hundred',
        6 => 'six hundred',
        7 => 'seven hundred',
        8 => 'eight hundred',
        9 => 'nine hundred',
    );

    private static $_tensName = array(
        0 => '',
        1 => 'ten',
        2 => 'twenty',
        3 => 'thirty',
        4 => 'fourty',
        5 => 'fifty',
        6 => 'sixty',
        7 => 'seventy',
        8 => 'eighty',
        9 => 'ninety',
    );
    private static $_oneName = array(
        0 => '',
        1 => 'one',
        2 => 'two',
        3 =>'three',
        4 =>'four',
        5 =>'five',
        6 =>'six',
        7 =>'seven',
        8 =>'eight',
        9 =>'nine',
        10 =>'ten',
        11 => 'eleven',
        12 => 'twelve',
        13 => 'thirteen',
        14 => 'fourteen',
        15 => 'fifteen',
        16 => 'sixteen',
        17 => 'seventeen',
        18 => 'eighteen',
        19 => 'nineteen',
    );
    private static $_categoryName = array(
        self::HUNDRED => array('','','',),
        self::THOUSAND => array('thousand','thousands',),
        self::MILLIONS => array('million','millions',),
    );

    protected function getCategoryName($one) {
        if (is_array(self::$_categoryName[$this->_category])) {
            if ($one == 1) {
                $tmp = self::$_categoryName[$this->_category][0];
            } else {
                $tmp = self::$_categoryName[$this->_category][1];
            }
            return $tmp;
        }
        throw new Exception('invalid getCategoryName'.__CLASS__);
    }
    protected function getHundredName($num) {
        if (isset(self::$_hundredName[$num])) {
            return self::$_hundredName[$num];
        }
        throw new Exception('invalid getHundredName '.__CLASS__);
    }
    protected function getTensName($num) {
        if (isset(self::$_tensName[$num])) {
            return self::$_tensName[$num];
        }
        throw new Exception('invalid getTensName'.__CLASS__);
    }
    protected function getOneName($num) {
        if (isset(self::$_oneName[$num])) {
            return self::$_oneName[$num];
        }
        throw new Exception('invalid getOneName'.__CLASS__);
    }
    public function getZero() {
        return 'zero';
    }
}
