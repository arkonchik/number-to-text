<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Arkon
 * Date: 17.07.12
 * Time: 22:05
 * To change this template use File | Settings | File Templates.
 */
class triple extends abstract_triple
{
    public function __construct( $str ) {
        if (strlen($str) > 3) {
            throw new Exception('invalid str lenght for three class');
        }
        $this->_number = $str;
        $this->getParts();
    }
}
