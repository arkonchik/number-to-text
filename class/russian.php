<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Arkon
 * Date: 17.07.12
 * Time: 20:38
 * To change this template use File | Settings | File Templates.
 */
require_once(DIR.'/class/abstract/langauge.php');
class russian extends language
{

    private static $_hundredName = array(
                                        0 => '',
                                        1 => 'сто',
                                        2 => 'двести',
                                        3 => 'триста',
                                        4 => 'четыреста',
                                        5 => 'пятьсот',
                                        6 => 'шестьсот',
                                        7 => 'семьсот',
                                        8 => 'восемьсот',
                                        9 => 'девятьсот',
                                    );
    private static $_tensName = array(
                                        0 => '',
                                        1 => 'десять',
                                        2 => 'двадцать',
                                        3 => 'тридцать',
                                        4 => 'сорок',
                                        5 => 'пятьдесят',
                                        6 => 'шестьдесят',
                                        7 => 'семьдесят',
                                        8 => 'восемьдесят',
                                        9 => 'девяносто',
                                    );
    private static $_oneName = array(
                                    0 => '',
                                    1 => array('одна', 'один',),
                                    2 => array('две', 'два',),
                                    3 =>'три',
                                    4 =>'четыре',
                                    5 =>'пять',
                                    6 =>'шесть',
                                    7 =>'семь',
                                    8 =>'восемь',
                                    9 =>'девять',
                                    10 =>'деcять',
                                    11 => 'одиннадцать',
                                    12 => 'двенадцать',
                                    13 => 'тринадцать',
                                    14 => 'четырнадцать',
                                    15 => 'пятнадцать',
                                    16 => 'шестнадцать',
                                    17 => 'семнадцать',
                                    18 => 'восемнадцать',
                                    19 => 'девятнадцать',
                                    );
    private static $_categoryName = array(
                                    self::HUNDRED => array('','','',),
                                    self::THOUSAND => array('тысяча','тысячи','тысяч',),
                                    self::MILLIONS => array('миллион','миллиона','миллионов',),
                                );
    private static $_categoryGender = array(
                                    self::HUNDRED => 1,
                                    self::THOUSAND => 0,
                                    self::MILLIONS => 1,
                                );

    protected function getCategoryName($one) {
        if (is_array(self::$_categoryName[$this->_category])) {
            if (in_array($one, array(2,3,4))) {
                $tmp = self::$_categoryName[$this->_category][1];
            } elseif ($one == 1) {
                $tmp = self::$_categoryName[$this->_category][0];
            } else {
                $tmp = self::$_categoryName[$this->_category][2];
            }
            return $tmp;
        }
        throw new Exception('invalid');
    }
    protected function getHundredName($num) {
        if (isset(self::$_hundredName[$num])) {
            return self::$_hundredName[$num];
        }
        throw new Exception('invalid getHundredName');
    }
    protected function getTensName($num) {
        if (isset(self::$_tensName[$num])) {
            return self::$_tensName[$num];
        }
        throw new Exception('invalid getTensName');
    }
    protected function getOneName($num) {
        if (isset(self::$_oneName[$num])) {
            if (!is_array(self::$_oneName[$num])) {
                return self::$_oneName[$num];
            } else {
                return self::$_oneName[$num][self::$_categoryGender[$this->_category]];
            }
        }
        throw new Exception('invalid getOneName');
    }
    public function getZero() {
        return 'ноль';
    }
}
