<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Arkon
 * Date: 17.07.12
 * Time: 20:39
 * To change this template use File | Settings | File Templates.
 */
require_once(DIR.'/class/english.php');
require_once(DIR.'/class/russian.php');
require_once(DIR.'/class/abstract/langauge.php');
require_once(DIR . '/class/abstract/triple.php');
require_once(DIR . '/class/triple.php');

class number
{
    private $_numder;
    private $_language;
    private $_tripleCollection = array();

    public function __construct($number = 0, language $lan) {
        $this->_numder = $number;
        $this->_language = $lan;
    }

    public function setLanguage(language $lan) {
        $this->_language = $lan;
    }

    public function getNumber2Text(){
        $this->_tripleCollection = array();
        if ( preg_match('/[^0-9]/', $this->_numder, $matches) ) {
            throw new Exception('not a number');
        }
        $numLen = strlen($this->_numder)-1;
        $numberRev = strrev($this->_numder);

        if ($this->_numder == 0) {
            return $this->_language->getZero();
        } else {
            for ($i = 0; $i <= $numLen; $i += 3) {
                $tmpStr = substr($numberRev, $i, 3);
                $tmpStr = strrev($tmpStr);
                $this->_tripleCollection[] = new triple($tmpStr);
            }
            return $this->_language->getWordName($this->_tripleCollection);//mb_convert_encoding($this->_language->getWordName($this->_numdersCollection), 'UTF-8');
        }
    }


}
